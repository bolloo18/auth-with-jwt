import express from "express";
import { config } from "dotenv";
import dbConnect from "./dbConnect.js"; 
import router  from "./routes/auth.js";
import refreshTokenRoutes from './routes/refreshToken.js'

const app = express();

config();
dbConnect();

app.use(express.json());
app.use('/api',router);
app.use('/api/refreshToken',refreshTokenRoutes);
const port = process.env.PORT || 8000;

app.listen(port, (err)=>{
    err && console.log(err.message);

    console.log(`App running on ${port}. press CTRL + C to stop`);
})
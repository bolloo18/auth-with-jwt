import { Router } from "express";
import UserToken from "../models/UserToken.js";
import jwt from 'jsonwebtoken';
import verifyRefreshToken from "../utils/verifyRefereshToken.js";
import Joi from "joi";
import { refereshTokenBodyValidation } from "../utils/validationSchema.js";

const refreshTokenRoutes = Router();

// @desc get new refresh tokens
// @route POST /
//access private
refreshTokenRoutes.post("/",async (req, res)=>{
    const {error} = refereshTokenBodyValidation(req.body.refreshToken);
    
    if(error) return res.status(400).json({error: true,message: error.details[0].message})

    console.log(req.body.refreshToken)

    verifyRefreshToken(req.body.refreshToken)
    .then(({tokenDetails})=>{
        const payload = {_id : tokenDetails._id, roles: tokenDetails.roles}
        const accessToken = jwt.sign(
            payload,
            process.env.REFRESH_TOKEN_SECRET,
            {expiresIn : "14m"}
            )
        return res.status(200).json({error : false, accessToken, message : "Access token created successfully"});
    })
    .catch(err=>{
        console.log(err)
        return res.status(400).json({err})
    });

     
});

// @Desc logout
// @route DElete /
// @access private
refreshTokenRoutes.delete("/",async (req,res)=>{
    try {
        const {error} = refereshTokenBodyValidation(req.body);

        if(error) return res.status(400).json({error: true,message: error.details[0].message});

        const userToken = await UserToken.findOne({token : req.body.refreshToken});

        if(!userToken) return res.status(200).json({error: false , message : "Logged Out Successfully"})
        
        await userToken.remove();
        return res.status(200).json({error: false , message : "Logged Out Successfully"});

    } catch (error) {
        console.log(error)
        res.status.json({error: true, message : "Internal serveer error"})
    }
});

export default refreshTokenRoutes;


import UserToken from "../models/UserToken.js";
import jwt from 'jsonwebtoken'; 

const verifyRefreshToken = (refreshToken)=>{

    const privateKey = process.env.REFRESH_TOKEN_SECRET;
    return new Promise((resolve,reject)=>{
        UserToken.findOne({token : refreshToken},(err,doc)=>{
            if(!doc){
                return reject({error: true, message: "Invalid refresh token"})
            }

            jwt.verify(refreshToken,privateKey,(err,tokeDetails)=>{
                if(err) return reject({error: true, message : "Invalid refresh token"});

                resolve({
                    tokeDetails,
                    error : false,
                    message : "Valid refresh token"
                })
            })
        })
    })

}

export default verifyRefreshToken;
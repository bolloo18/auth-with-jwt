import Joi from 'joi';
import passwordComplexity from 'joi-password-complexity';

const signupBodyValidation = (body)=>{
    const schema = Joi.object({
        email: Joi.string().required().label("Email"),
        userName : Joi.string().required().label("UserName"),
        password : passwordComplexity().required().label("Password")
    })
    return schema.validate(body);
}

const loginBodyValidation = (body)=>{
    const schema = Joi.object({
        email: Joi.string().required().label("Email"),
        password : Joi.string().required().label("Password"),
    });

    return schema.validate(body);
}

const refereshTokenBodyValidation = (body)=>{

    const schema = Joi.object({
        refreshToken : Joi.string().required().label("RefreshTokens") 
    })

    return schema.validate(body);
}

export {signupBodyValidation, loginBodyValidation, refereshTokenBodyValidation};